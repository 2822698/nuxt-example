import citiesList from '@/data.json';
import {calcPagesCount, checkCurrentLastPage} from './tools';

export const state = () => ({
	cities: citiesList,
	page: 0,
	perPage: 2
});

export const getters = {
	pager: ({page, perPage, cities}) => ({
		page: page,
		perPage: perPage,
		pagesCount: calcPagesCount(cities.length, perPage)
	}),

	citiesOnPage: ({page, perPage, cities}) => cities.slice(page * perPage, (page + 1) * perPage)
};

export const actions = {
	setPage({commit, getters}, page) {
		if (page < getters.pager.pagesCount && page >= 0)
			commit('SET_PAGE', page);
	},

	deleteCity({commit, state}, cityName) {
		// todo: отправка реквеста на сервер
		// проверка на пустую страницу
		const newPage = checkCurrentLastPage(state.page, state.perPage, state.cities.length);
		commit('SET_PAGE', newPage);
		commit('DELETE_CITY', cityName);
	},

	addCity({commit}, city) {
		// todo: отправка реквеста на сервер
		commit('ADD_CITY', city);
	}
};

export const mutations = {
	SET_PAGE(state, page) {
		state.page = page;
	},

	DELETE_CITY(state, cityName) {
		const index = state.cities.findIndex(el => el.name === cityName);
		if (index < 0) return;

		state.cities.splice(index, 1);
	},

	ADD_CITY(state, city) {
		state.cities.push(city);
	}
};
