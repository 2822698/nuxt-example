export function calcPagesCount(count, perPage) {
	return Math.ceil(count / perPage);
}

export function checkCurrentLastPage(page, perPage, count) {
	const newPagesCount = calcPagesCount(count - 1, perPage);
	if (newPagesCount > 0 && newPagesCount <= page + 1) {
		return newPagesCount - 1;
	}
	return page;
}
